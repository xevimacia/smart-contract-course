from scripts.helpful_scripts import get_account
from brownie import marketPlaceBoilerPlate, network, config

def deploy():
    account = get_account()
    marketPlace = marketPlaceBoilerPlate.deploy(
        {"from": account}
    )
    print("Deployed marketPlace!")
    return marketPlace

def main():
    deploy()
