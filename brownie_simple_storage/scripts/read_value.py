from brownie import accounts, config, network, SimpleStorage


def read_contract():
    simple_storage = SimpleStorage[-1]  # 0 = first deployment / -1 = latest deployment
    # go take the index that's one less than the length
    # Brownie already knows the ABI and address (no need to get it)
    print(simple_storage.retrieve())


def main():
    read_contract()
