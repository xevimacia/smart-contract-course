from brownie import accounts, config, network, SimpleStorage

# import os # used to get env variables

# Run brownie scripts:
# brownie run scripts/deploy.py
# Check available network list:
# brownie networks list
# Deploy in Eth using infura:
# brownie run scripts/deploy.py --network ropsten

# Account managment with brownie CLI:
# Add account (password encrypted):
# brownie accounts new {name of the account}
# List accounts:
# brownie accounts list
# Delete account:
# brownie accounts delete {name of the account}

# In terminal we can use brownie console to interact directly with the contract:
# brownie console
# Close by:
# quit()


def deploy_simple_storage():
    # use brownie provided accounts
    # account = accounts[0]
    # print(account)

    # use account saved in brownie (password protected)
    # account = accounts.load("metamask-account")
    # print(account)

    # use account saved in .env using os.getenv
    # account = accounts.add(os.getenv("PRIVATE_KEY"))
    # print(account)

    # a more elegant way is using brownie-config.yaml
    # account = accounts.add(config["wallets"]["from_key"])
    # print(account)

    # Adding account dinamically
    account = get_account()

    # Deploy a contract
    simple_storage = SimpleStorage.deploy({"from": account})
    stored_value = simple_storage.retrieve()
    print(stored_value)
    transaction = simple_storage.store(15, {"from": account})
    transaction.wait(1)
    updated_stored_value = simple_storage.retrieve()
    print(updated_stored_value)


def get_account():
    if network.show_active() == "development":
        return accounts[0]
    else:
        return accounts.add(config["wallets"]["from_key"])


def main():
    deploy_simple_storage()
