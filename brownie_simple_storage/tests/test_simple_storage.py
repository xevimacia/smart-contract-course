from brownie import SimpleStorage, accounts

# Testing our contracts CLI:
# Full tests: brownie test
# Test one function: brownie test -k test_updating_storage
# Test function with shell to debug: brownie test --pdb
# Test function verbose: brownie test -s


def test_deploy():
    # Initial value test
    # Arrange
    account = accounts[0]
    # Act
    simple_storage = SimpleStorage.deploy({"from": account})
    starting_value = simple_storage.retrieve()
    expected = 0
    # Assert
    assert starting_value == expected


def test_updating_storage():
    # Updating value test
    # Arrange
    account = accounts[0]
    simple_storage = SimpleStorage.deploy({"from": account})
    # Act
    expected = 15
    simple_storage.store(expected, {"from": account})
    # Assert
    assert expected == simple_storage.retrieve()
